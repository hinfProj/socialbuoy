import { Injectable } from '@angular/core';
import {AlertController} from 'ionic-angular';
import firebase from 'firebase';
import { Events } from 'ionic-angular';
import {connreq} from "../../models/interfaces/request";
import { UserProvider } from '../user/user';
import { AngularFireAuth } from 'angularfire2/auth';
/*
  Generated class for the RequestsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RequestsProvider {
  firereq = firebase.database().ref('/requests');
  firefriends = firebase.database().ref('/friends');
  fireblockuser = firebase.database().ref('/blockedUser');
  userdetails;
  myfriends;

  constructor(public userservice: UserProvider, public events: Events,
              public afireauth:AngularFireAuth, public alertCtrl: AlertController) {
  }

  //Send friend request to user, if the user has blocked you then alert message is triggered.
  blockedRequestSent(req:connreq){
    let blockedUserId = req.sender;
    let recipientId = req.recipient;
    let blockAlert = this.alertCtrl.create({
        title: 'Request send failed',
        subTitle: 'The user has blocked you hence you cannot chat him/her',
        buttons: ['Ok']

    });
    let promise = new Promise((resolve, reject) =>{
    this.fireblockuser.child(recipientId).orderByChild('blockedUserUID').on('value', (snapshot)=>{
     let dataSnap = snapshot.val();
     if(dataSnap == null){
         console.log("Request sent");
          this.firereq.child(req.recipient).push({
            sender: req.sender
          }).then(() => {
            resolve({success: true});
         });
     }
     else {
       let dumps = [];
       let tempArray = Object.keys(dataSnap);
       for (let i = 0; i < tempArray.length; i++) {
         let k = tempArray[i];
         let blockedIds = dataSnap[k].blockedUserUID;
         if (blockedIds === blockedUserId) {
           dumps.push(blockedIds);
           console.log("Dumps array content! " + dumps);
         }
       }//for..
       console.log(dumps);
       if (dumps[0] == blockedUserId) {
         blockAlert.present();
       }
       else {
         // console.log("Request sent");
         this.firereq.child(req.recipient).push({
           sender: req.sender
         }).then(() => {
           resolve({success: true});
         })
       }
     }
     })
    });
    return promise;
  }



  deletefriend(buddy) {
    let promise = new Promise((resolve, reject) => {
     this.firefriends.child(firebase.auth().currentUser.uid).orderByChild('uid').equalTo(buddy.uid).once('value', (snapshot) => {
          let somekey;
          for (let key in snapshot.val())
            somekey = key;
          console.log(somekey);
          this.firefriends.child(firebase.auth().currentUser.uid).child(somekey).remove().then(() => {
            resolve(true);
          })
         }).then(() => {
        }).catch((err) => {
          reject(err);
        })
    });
    return promise;
  }


  getmyrequests() {
    let allmyrequests;
    let myrequests = [];
    this.firereq.child(firebase.auth().currentUser.uid).on('value', (snapshot) => {
      allmyrequests = snapshot.val();
      myrequests = [];
      for (let i in allmyrequests) {
        myrequests.push(allmyrequests[i].sender);
      }
      this.userservice.getallusers().then((res) => {
        let allusers = res;
        this.userdetails = [];
        for (let j in myrequests)
          for (let key in allusers) {
            if (myrequests[j] === allusers[key].uid) {
              this.userdetails.push(allusers[key]);
            }
          }
        this.events.publish('gotrequests');
      })
    })
  }


  acceptrequest(buddy) {
    var promise = new Promise((resolve, reject) => {
      this.firefriends.child(firebase.auth().currentUser.uid).push({
        uid: buddy.uid
      }).then(() => {
        this.firefriends.child(buddy.uid).push({
          uid: firebase.auth().currentUser.uid
        }).then(() => {
          this.deleterequest(buddy).then(() => {
            resolve(true);
          })
        })
      })
    });
    return promise;
  }


  deleterequest(buddy) {
    let promise = new Promise((resolve, reject) => {
     this.firereq.child(firebase.auth().currentUser.uid).orderByChild('sender').equalTo(buddy.uid).once('value', (snapshot) => {
          let somekey;
          for (let key in snapshot.val())
            somekey = key;
          this.firereq.child(firebase.auth().currentUser.uid).child(somekey).remove().then(() => {
            resolve(true);
          })
         })
          .then(() => {
        }).catch((err) => {
          reject(err);
        })
    });
    return promise;
  }


  getmyfriends() {
    let friendsuid = [];
    this.firefriends.child(firebase.auth().currentUser.uid).on('value', (snapshot) => {
      let allfriends = snapshot.val();
      this.myfriends = [];
      for (let i in allfriends)
        friendsuid.push(allfriends[i].uid);
      this.userservice.getallusers().then((chatusers) => {
        this.myfriends = [];
        for (let j in friendsuid)
          for (let key in chatusers) {
            if (friendsuid[j] === chatusers[key].uid) {
              this.myfriends.push(chatusers[key]);
            }
          }
        this.events.publish('friends');
      }).catch((err) => {
        alert(err);
      })

    })
  }


  blockfriend(item){
    var promise = new Promise((resolve, reject) => {
      this.fireblockuser.child(this.afireauth.auth.currentUser.uid).push({
        blockedUserUID: item.uid
      }).then(() => {
        resolve({success: true});
      })
    });
    return promise;
  }



}
