import { Injectable } from '@angular/core';
import firebase from 'firebase';
import { Events } from 'ionic-angular';

@Injectable()
export class ChatProvider {
  buddy:any;
  buddymessages=[];
  icon:any;
  firedata = firebase.database().ref('/chatusers');
  fireoneToOneChat = firebase.database().ref('/oneToOneChats');
  constructor(public events:Events) {
  }

  initializebuddy(buddy){
    this.buddy = buddy;
  }


  addnewmessage(msg) {
    if (this.buddy) {
      let promise = new Promise((resolve, reject) => {
        this.fireoneToOneChat.child(firebase.auth().currentUser.uid).child(this.buddy.uid).push({
          sentby: firebase.auth().currentUser.uid,
          message: msg,
          timestamp: firebase.database.ServerValue.TIMESTAMP
        }).then(() => {
          this.fireoneToOneChat.child(this.buddy.uid).child(firebase.auth().currentUser.uid).push({
            sentby: firebase.auth().currentUser.uid,
            message: msg,
            timestamp: firebase.database.ServerValue.TIMESTAMP
          }).then(() => {
            resolve(true);
            })
        })
      });
      return promise;
    }
  }


  getbuddymessages(){
    let temp;
    this.fireoneToOneChat.child(firebase.auth().currentUser.uid).child(this.buddy.uid).on('value', (snapshot)=>{
      this.buddymessages=[];
      temp = snapshot.val();
      for(let key in temp){
        this.buddymessages.push(temp[key]);
      }
        this.events.publish('newmessage');
    })
  }






}
