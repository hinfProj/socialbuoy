import {Component, Injectable} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CreatepostPage } from '../createpost/createpost';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserProvider } from '../../providers/user/user';
import {createElement} from "@angular/core/src/view/element";
// import {AngularFireDatabase, AngularFireList} from "angularfire2/database";


@IonicPage()
@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})

export class PostPage {
  myArray:any=[];
  data:any;
  itemSearchList:any=[];
  firedata = firebase.database().ref('/chatusers');


  constructor(public navCtrl: NavController, public navParams: NavParams, public storage:Storage,
              public userservice: UserProvider, public afireauth: AngularFireAuth) {
            this.initializeItems();
  }


  ionViewDidLoad() {
  this.getPostsToRender();

  }


  initializeItems() {
    this.itemSearchList = this.myArray;
  }


  //Getting all the posts with the person nickname from firebase database
  // and pushing the title and display name to array to render in view
  getPostsToRender(){
    this.userservice.getAllPosts().on('value', (snapshot) => {
      let posts = snapshot.val();
      let keys = Object.keys(posts);
      for (let i = 0; i < keys.length; i++) {
        let k = keys[i];
        let postArray = posts[k].post;
        if (postArray != null){
          let NickName = posts[k].displayName;
          let uid = posts[k].uid;
          for (let j = 0; j < postArray.length; j++) {
            let itemList = postArray[j].title;
            let desc = postArray[j].desc;
            let arrayObj = {displayName: NickName, item: itemList, desc:desc, uid:uid};
            this.myArray.push(arrayObj);
            // console.log(uid + " : " + NickName + " : " + itemList + " : " + desc);
          }
        }
      }
    });//End of function ionViewDidLoad()
  }

  //Search Bar logic
  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.itemSearchList = this.itemSearchList.filter((item) => {
        return (item.item.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  //Navigating to CreatepostPage
  createPost(){
    this.navCtrl.push('CreatepostPage')
  }

  //Get user data when clicked on small chat button on post
  userProfilePost(userTitle, userDescription, UserDisplayName, UserUID){
    let data = {
      title : userTitle,
      description : userDescription,
      displayName : UserDisplayName,
      uid:UserUID
  };
    // this.navCtrl.push('ChatsPage', data);
    this.navCtrl.push('UserProfilePage', data)
  }




}
