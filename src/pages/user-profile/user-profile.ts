import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { ChatProvider } from '../../providers/chat/chat';
import { connreq } from '../../models/interfaces/request';
import * as firebase from "firebase";
import { RequestsProvider } from '../../providers/requests/requests';


@IonicPage()
@Component({
  selector: 'page-user-profile',
  templateUrl: 'user-profile.html',
})
export class UserProfilePage {
  tabBarElement:any;
  newrequest = {} as connreq;
  nickname:any;
  PostTitle:any;
  PostDescription:any;
  UserID:any;
  showLogin:boolean;
  counter:any = 0;
  requestSent:boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public afireauth: AngularFireAuth,
              public chatservice:ChatProvider, public alertCtrl: AlertController,
              public requestservice: RequestsProvider) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

  }

  ionViewDidLoad(){
    this.PostTitle = this.navParams.get('title');
    this.PostDescription = this.navParams.get('description');
    this.nickname = this.navParams.get('displayName');
    this.UserID = this.navParams.get('uid');
    this.checkCurrentUser();

  }

  ionViewWillEnter(){
    this.tabBarElement.style.display = 'none';
  }
  ionViewWillLeave(){
    this.tabBarElement.style.display = 'flex';
  }

  //Check if the logged in user and the current post user are same or different
  checkCurrentUser(){
    let LoggedInUID = this.afireauth.auth.currentUser.uid;
    let currentUID = this.UserID;
    if( LoggedInUID === currentUID){
      this.showLogin=true;
    }
    else{
      this.showLogin=false;
    }
  }

  //Sending request to recipient with the UID
  sendreq(id) {

    this.newrequest.sender = this.afireauth.auth.currentUser.uid;
    this.newrequest.recipient = id;
    let successalert = this.alertCtrl.create({
        title: 'Request sent',
        subTitle: 'Your request was sent to ' + this.nickname,
        buttons: ['ok']

    });
    let issueAlert = this.alertCtrl.create({
        title: 'Request sent Already',
        subTitle: 'Your request was sent already to ' + this.nickname,
        buttons: ['cancle']
    });

    this.requestservice.blockedRequestSent(this.newrequest).then((res: any) =>
    {
     if(this.UserID == id){
          this.counter++;
          if (res.success && this.counter==1) {successalert.present();}
          else{issueAlert.present();}
     }
     })
  }
}
