import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import firebase from 'firebase';

/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
  displayName: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public userservice: UserProvider) {

  }

  ionViewWillEnter() {
    this.loaduserdetails();
  }

  loaduserdetails(){
    this.userservice.getuserdetails().then((res:any)=>{
      this.displayName = res.displayName;
    })
  }

  logout(){
    firebase.auth().signOut().then(()=>{
      this.navCtrl.parent.parent.setRoot('LoginPage');
    })
  }

}
