import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { usercreds } from '../../models/interfaces/usercreds';
import { AuthProvider } from '../../providers/auth/auth';
import firebase from 'firebase';
import { DomSanitizer } from '@angular/platform-browser';


@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  urlImage:any;
  firestorage = firebase.storage().ref().child('bg.jpg');
  credentials = {} as usercreds;
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public authservice: AuthProvider, public loadingCtrl: LoadingController,
              private _sanitizer: DomSanitizer) {
    this.firestorage.getDownloadURL().then((url) =>{
      this.urlImage = url;
    });
  }
  public sanitizeImage(image: string) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

   ionViewDidLoad() {

   }

  signin(){
   let loader = this.loadingCtrl.create({
      content: 'Please wait'
    });
    loader.present();
    this.authservice.login(this.credentials).then((res:any)=>{
      if(!res.code)
        this.navCtrl.setRoot('TabsPage');
      else alert(res);
    });
    loader.dismiss();
  }
  signup(){
    this.navCtrl.push('SignupPage');
  }

}
