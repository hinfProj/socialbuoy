import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { Storage } from '@ionic/storage';
import { DomSanitizer } from '@angular/platform-browser';
import firebase from 'firebase';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  newuser={
    email: '',
    password:'',
    displayName:''
  };
  urlImage:any;
  firestorage = firebase.storage().ref().child('bg.jpg');

  constructor(public navCtrl: NavController, public navParams: NavParams, public userservice: UserProvider,
              public loadingCtrl: LoadingController, public toastCtrl: ToastController,
              public storage:Storage, private _sanitizer: DomSanitizer) {
    this.firestorage.getDownloadURL().then((url) =>{
      this.urlImage = url;
    });

  }

  public sanitizeImage(image: string) {
    return this._sanitizer.bypassSecurityTrustStyle(`url(${image})`);
  }

  signup(){
    var toaster = this.toastCtrl.create({
      duration:3000,
      position:'bottom'
    });

    if(this.newuser.email==''|| this.newuser.password=='' || this.newuser.displayName==''){
      toaster.setMessage("All fields are required");
      toaster.present();
    }
    else if(this.newuser.password.length<7){
      toaster.setMessage('Password is not strong. Try giving more than six characters');
      toaster.present();
    }
    else {
      let loader = this.loadingCtrl.create({
        content: 'Please wait'
      });
      loader.present();
      this.userservice.adduser(this.newuser).then((res: any)=>{
      loader.dismiss();
      if(res.success){
        this.navCtrl.push('TabsPage');
      }
      else{
        alert("Error" + res);
      }
      })
    }
    // this.navCtrl.push('ProfilePage',this.storage.set('myData', this.newuser.displayName) )
  }
  goback(){
    this.navCtrl.setRoot('LoginPage');
  }

}
