import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from "firebase";
import {UserProvider} from "../../providers/user/user";

@IonicPage()
@Component({
  selector: 'page-viewprofile',
  templateUrl: 'viewprofile.html',
})
export class ViewprofilePage {
   tabBarElement:any;
     UserID:any;
     nickname:any;
     titleArray:any=[];
     firerateUser = firebase.database().ref('/ratings');
     averageRating:any;
     ratingsArray:any=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public afireauth: AngularFireAuth, public userservice:UserProvider,
              public alertCtrl: AlertController) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  ionViewDidLoad() {
        this.nickname = this.navParams.get('displayName');
        this.UserID = this.navParams.get('uid');
        this.getUserDetails();
        this.getRatingsForUser();
  }

  ionViewWillEnter(){
    this.tabBarElement.style.display = 'none';
  }
  ionViewWillLeave(){
    this.tabBarElement.style.display = 'flex';
  }

  getUserDetails(){
    let LoggedInUID = this.afireauth.auth.currentUser.uid;
    let currentUID = this.UserID;
    this.userservice.getAllPosts().on('value', (snapshot) => {
      let snapData = snapshot.val();
      let keys = Object.keys(snapData);
      for (let i = 0; i < keys.length; i++) {
         if(keys[i] == currentUID ){
           let postObj = snapData[keys[i]].post;
           for(let j=0; j<postObj.length; j++){
             this.titleArray.push(postObj[j].title);
           }
         }
      }
  })
}

  rateUser(radio){
    let postCreated = this.alertCtrl.create({
    title: 'Ratings saved',
    subTitle: 'Thank you for rating the user',
    buttons: [{
      text:'Ok',
      handler:()=>{
        this.navCtrl.push('ChatsPage');
        this.ratingsGiven(radio);
      }
    }]
    });
    postCreated.present();
  }

  ratingsGiven(rating){
    let LoggedInUID = this.afireauth.auth.currentUser.uid;
    let currentUID = this.UserID;
    let promise = new Promise((resolve, reject) =>{
    this.firerateUser.child(currentUID).push({
      ratingsText:rating,
      fromUID:LoggedInUID
    }).then(() => {
      resolve({success: true});
    })
    });
  }

  getRatingsForUser(){
    let LoggedInUID = this.afireauth.auth.currentUser.uid;
    let currentUID = this.UserID;
    let v1,v2,v3;
    let dumspsArray=[];
    this.firerateUser.child(currentUID).on('value', (snapshot)=>{
      let snapData = snapshot.val();
      if (snapData === null || snapData==='undefined'){
        console.log("Empty array");
      }
      else {
        let keys = Object.keys(snapData);
        for (let i = 0; i < keys.length; i++) {
          let rateObj = snapData[keys[i]].ratingsText;
          if (rateObj == "bad") {
            v1 = 1;
            dumspsArray.push(v1);
          }
          else if (rateObj == "good") {
            v2 = 2;
            dumspsArray.push(v2);
          }
          else {
            v3 = 3;
            dumspsArray.push(v3);
          }
        }
        //Calulate the rating for the user
        this.calcAvgRatings(dumspsArray);
      }
    })

  }


  calcAvgRatings(ratingArray){
      this.ratingsArray = ratingArray;
      let sum = 0;
      let avg;
      let roundedAvg;
      for (let i = 0; i < ratingArray.length; i++) {
        sum = sum + ratingArray[i];
      }
      avg = sum / ratingArray.length;
      roundedAvg = Math.round(avg * 100) / 100;
      this.averageRating = roundedAvg;
      return this.averageRating;
    }


}//end of class
