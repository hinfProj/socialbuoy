import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Events, AlertController, ToastController} from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { RequestsProvider } from '../../providers/requests/requests';

/**
 * Generated class for the ChatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html',
})
export class ChatsPage {
  firedata = firebase.database().ref('/chatusers');
  chatdata = firebase.database().ref('/chatstorage');
  buddy:any;
  SenderUser:any;
  ReceiverUser:any;
  myrequests;
  myfriends;
  flag:boolean=true;

  constructor(public navCtrl: NavController, public navParams: NavParams,
              public chatservice:ChatProvider, public afireauth: AngularFireAuth,
              public requestservice: RequestsProvider,
              public events: Events, public alertCtrl: AlertController,
              public toastCtrl: ToastController) {

    this.buddy = this.chatservice.buddy;
    //Get UID of sender and receiver
    this.SenderUser = this.afireauth.auth.currentUser.uid;
    this.ReceiverUser = this.navParams.get('uid');
  }

  ionViewWillEnter(){
    this.requestservice.getmyrequests();
    this.requestservice.getmyfriends();
    this.myfriends = [];

    this.events.subscribe('gotrequests', () => {
      this.myrequests = [];
      this.myrequests = this.requestservice.userdetails;
    });
    this.events.subscribe('friends', () => {
      this.myfriends = [];
      this.myfriends = this.requestservice.myfriends;
    })

  }
  ionViewWillLeave(){
    this.events.unsubscribe('gotrequests');
    this.events.unsubscribe('friends');
  }

  ionViewDidLoad() {
  }

  accept(item) {
    this.requestservice.acceptrequest(item).then(() => {
      let acceptAlert = this.alertCtrl.create({
        title: 'Friend added',
        subTitle: 'Tap on the chat button to connect and chat with him',
        buttons: ['Okay']
      });
      acceptAlert.present();
    })
  }

  ignore(item) {
    console.log(item);
    let ignoreAlert = this.alertCtrl.create({
        title: 'Friend Request declined',
        subTitle: 'You have successfully declined ' + item.displayName +"'s"+ ' friend request',
        buttons: ['Okay']
      });
    this.requestservice.deleterequest(item).then(() => {
       ignoreAlert.present();
    }).catch((err) => {
      alert(err);
    })
  }

  viewProfile(item){
    let data = {
      displayName : item.displayName,
      uid:item.uid
  };
    this.navCtrl.push('ViewprofilePage', data);
  }

  buddychat(buddy){
    this.chatservice.initializebuddy(buddy);
    this.navCtrl.push('BuddiesPage');
  }

  //Block the user
  blockuser(item) {
   let blockAlert = this.alertCtrl.create({
        title: item.displayName + " Blocked!",
        subTitle: item.displayName + ' wont be able to send you any more requests.',
        buttons: ['Okay']
      });
    this.requestservice.deletefriend(item).then(() => {
      console.log("Deleted from friend list");
    });
    this.requestservice.blockfriend(item).then(() => {
      blockAlert.present();
    });
  }

}//end of class
