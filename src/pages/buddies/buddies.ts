import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content } from 'ionic-angular';
import { ChatProvider } from '../../providers/chat/chat';
import * as firebase from "firebase";
/**
 * Generated class for the BuddiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-buddies',
  templateUrl: 'buddies.html',
})
export class BuddiesPage {
  @ViewChild('content') content:Content;
  newmessage;
  buddy:any;
  icon:any;
  allmessages = [];
  firestorage = firebase.storage().ref().child('chaticon.png');
  constructor(public navCtrl: NavController, public navParams: NavParams,
              public chatservice:ChatProvider, public events:Events) {
    this.buddy = this.chatservice.buddy;
    this.scrollToBottom();
    this.events.subscribe('newmessage', ()=>{
      this.allmessages = [];
      this.allmessages = this.chatservice.buddymessages;
    })

  }

  ionViewDidLoad() {
  }
  ionViewDidEnter(){
    this.chatservice.getbuddymessages();
    this.getChatIcon();
  }

  addmessage(){
    this.chatservice.addnewmessage(this.newmessage).then(()=>{
      this.scrollToBottom();
      this.newmessage ='';

    });
  }

  getChatIcon(){
    this.firestorage.getDownloadURL().then((url) =>{
      this.icon = url;
      return this.icon;
    });
  }

  scrollToBottom(){
    setTimeout(() =>{
      this.content.scrollToBottom();
    }, 1000);
  }

}
