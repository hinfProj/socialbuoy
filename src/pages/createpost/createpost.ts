import {Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserProvider } from '../../providers/user/user';


@IonicPage()
@Component({
  selector: 'page-createpost',
  templateUrl: 'createpost.html',

})
export class CreatepostPage {
  userdata={
    title: '',
    description:''
  };
   firedata = firebase.database().ref('/chatusers/');
   displayName:string;
  tabBarElement:any;
  constructor(public navCtrl: NavController, public toastCtrl: ToastController,
              public storage:Storage, public userservice: UserProvider,
              public afireauth: AngularFireAuth, public AlertCtrl:AlertController) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  };

  ionViewWillEnter(){
    this.tabBarElement.style.display = 'none';
  }
  ionViewWillLeave(){
    this.tabBarElement.style.display = 'flex';
  }


  loaduserdetails(){
    this.userservice.getuserdetails().then((res:any)=>{
      this.displayName = res.displayName;
    })
  }

  postData(title, desc){
    let uniqueUID = this.afireauth.auth.currentUser.uid;
    let tempArr = [];
    let postCreated = this.AlertCtrl.create({
        title: 'Post Created',
        subTitle: 'You have successfully created the post',
        buttons: ['Ok']

    });
    let toaster = this.toastCtrl.create({
      duration: 3000,
      position: 'bottom'
    });

    if(this.userdata.title==""){
      toaster.setMessage("Please fill out the title");
      toaster.present();
    }

    //Storage used to move around the tabs with the data locally stored
    this.storage.get('Task1-'+uniqueUID).then((item)=> {
      if (item != null) {
        item.push({title, desc});
        this.storage.set('Task1-'+uniqueUID, item);
        //Updating database in firebase with new values
        this.firedata.child(this.afireauth.auth.currentUser.uid).update({
          uid: this.afireauth.auth.currentUser.uid,
          post: item,
          displayName: this.afireauth.auth.currentUser.displayName
        });
      }
      else{
        tempArr.push({title, desc});
        this.storage.set('Task1-'+uniqueUID, tempArr);
        this.firedata.child(this.afireauth.auth.currentUser.uid).update({
          uid: this.afireauth.auth.currentUser.uid,
          post: tempArr,
          displayName: this.afireauth.auth.currentUser.displayName
        });
      }
    });
    this.navCtrl.push('PostPage');
    postCreated.present();
  }
}
