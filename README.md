# Instructions to run the application #

## Introduction ##

The application runs on two platforms, locally on your computer or can be installed on your android devices by installing the APK file provided by us. **The recommended solution is to install the APK file on your android devices and get it running.** 

The Application is built using Ionic3, its a web framework which is used to develop mobile applications and Angular4. To read more about what Ionic, here is a starter template which will help you get an insight on the same: http://ionicframework.com/docs/

## Instructions to run on Android Devices ##
**
The APK link:- [https://drive.google.com/open?id=1Wqb8jY7W1wOIs_KG8Tt_xHdpkMj0PG_n](https://drive.google.com/open?id=1Wqb8jY7W1wOIs_KG8Tt_xHdpkMj0PG_n)**
